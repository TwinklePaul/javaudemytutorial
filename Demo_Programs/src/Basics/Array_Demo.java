package Basics;

public class Array_Demo {

	public static void main(String[] args) {

		// Simple Arrays
		int value = 7;

		int[] values;
		values = new int[3];

		System.out.println(values[0]);

		values[0] = 10;
		values[1] = 20;
		values[2] = 30;

		System.out.println(values[0]);
		System.out.println(values[1]);
		System.out.println(values[2]);

		for (int i = 0; i < values.length; i++) {
			System.out.println(values[i]);
		}

		int[] numbers = { 5, 6, 7 };

		for (int i = 0; i < numbers.length; i++) {
			System.out.println(numbers[i]);
		}
		
		
		
		//Array of Strings
		// Declare array of (references to) strings.
        String[] words = new String[3];
         
        // Set the array elements (point the references
        // at strings)
        words[0] = "Hello";
        words[1] = "to";
        words[2] = "you";
         
        // Access an array element and print it.
        System.out.println(words[2]);
         
        // Simultaneously declare and initialize an array of strings
        String[] fruits = {"apple", "banana", "pear", "kiwi"};
         
        // Iterate through an array
        for(String fruit: fruits) {
            System.out.println(fruit);
        }
         
        // "Default" value for an integer
        int data = 0;
         
        // Default value for a reference is "null"
        String text = null;
         
        System.out.println(text);
         
        // Declare an array of strings
        String[] texts = new String[2];
         
        // The references to strings in the array
        // are initialized to null.
        System.out.println(texts[0]);
         
        // ... But of course we can set them to actual strings.
        texts[0] = "one";
        
        
        
        //Multidimensional Arrays
        // 1D array
        int[] multi_dimen = {3, 5, 2343};
         
        // Only need 1 index to access values.
        System.out.println(multi_dimen[2]);
         
        // 2D array (grid or table)
        int[][] grid = {
            {3, 5, 2343},
            {2, 4},
            {1, 2, 3, 4}
        };
         
        // Need 2 indices to access values
        System.out.println(grid[1][1]);
        System.out.println(grid[0][2]);
         
        // Can also create without initializing.
        String[][] texts_ = new String[2][3];
         
        texts_[0][1] = "Hello there";
         
        System.out.println(texts_[0][1]);
         
        // How to iterate through 2D arrays.
        // first iterate through rows, then for each row
        // go through the columns.
        for(int row=0; row < grid.length; row++) {
            for(int col=0; col < grid[row].length; col++) {
                System.out.print(grid[row][col] + "\t");
            }
             
            System.out.println();
        }
         
        // The last array index is optional.
        String[][] words_ = new String[2][];
         
        // Each sub-array is null.
        System.out.println(words_[0]);
         
        // We can create the subarrays 'manually'.
        words_[0] = new String[3];
         
        // Can set a values in the sub-array we
        // just created.s
        words_[0][1] = "hi there";
         
        System.out.println(words_[0][1]);

	}

}
