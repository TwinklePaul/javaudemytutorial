package Basics;

import java.util.Scanner;

public class Loops_n_Inputs {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter a string, followed by an integer and a floating point number:");
		
		String line = input.nextLine();
		int number = input.nextInt();
		double value = input.nextDouble();
		
		System.out.printf("Inputted values are:\n%s\n%d\n%f\n\n",line, number, value);
		
		int count = 0;
		while(number != 10) {
			System.out.println("Enter an integer:");
			number = input.nextInt();
			count++;
		}
		
		if (count <= 3)
			System.out.printf("\n\nCongratulations! You have entered the lucky number in %d attempts.\n",count);
		else
			System.out.println("\n\nSorrys! You have crossed the attempt-limit.");
			
	}

}
