package Interfaces_Demo;
public interface IStartable {
    public void start();
    public void stop();
}