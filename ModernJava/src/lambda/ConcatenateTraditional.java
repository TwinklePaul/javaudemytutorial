package lambda;

public class ConcatenateTraditional implements ConcatenateInterface {
	@Override
	public String sconcat(String a, String b) {
		return a + " " + b;
	}

	public static void main(String[] args) {
		ConcatenateTraditional concetanateTraditional = new ConcatenateTraditional();
		System.out.println(concetanateTraditional.sconcat("Hello", "World"));

	}
}