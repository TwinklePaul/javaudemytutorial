package lambda;

public class ConcatenateWithLambda {

	public static void main(String[] args) {
		
		ConcatenateInterface cin = (a, b) -> a + " " + b;
		System.out.println(cin.sconcat("Hello", "World"));

	}

}
