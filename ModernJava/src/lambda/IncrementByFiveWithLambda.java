package lambda;

public class IncrementByFiveWithLambda {
	
	public static void main(String []args) {
		IncrementByFiveInterface incrby5 = (x) -> x + 5;
		System.out.println(incrby5.incrementByFive(2));
	}

}
