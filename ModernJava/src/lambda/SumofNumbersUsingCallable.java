package lambda;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

public class SumofNumbersUsingCallable {
	public static int[] array = IntStream.rangeClosed(0, 5000).toArray();
	public static int total = IntStream.rangeClosed(0, 5000).sum();

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		//Callable Thread 1 -> Sum of 1st 2500
		Callable callable1 = () -> {
			int sum = 0;
			
			for (int i = 0; i < array.length/2; i++)
				sum += array[i];
			
			return sum;
		};
		
		//Callable Thread 2 -> Sum of next 2500
		Callable callable2 = () -> {
			int sum = 0;
			
			for (int i = array.length/2; i < array.length; i++)
				sum += array[i];
			
			return sum;
		};
		
		//ExecuterService is used to run threads
		ExecutorService execService = Executors.newFixedThreadPool(2);
		
		
		List<Callable<Integer>> taskList = Arrays.asList(callable1, callable2);
		List<Future<Integer>> results = execService.invokeAll(taskList);
		
		int k = 0;
		int sum = 0;
		
		for (Future<Integer> result: results) {
			sum = sum + result.get();
			System.out.println("Sum of "+ ++k + " is: "+ result.get());
		}
		
		System.out.println("Sum from the Callable is: " + sum);
		System.out.println("Correct sum from IntStream is: " + total);
		
		execService.shutdown();
	}

}
